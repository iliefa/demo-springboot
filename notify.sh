#!/bin/bash
# vim: set tabstop=4 expandtab:
set -e
set -u

####### 1. - MAIN BLOCK ########################################################################################################################################

function generateImageUniqueID ()
{

    # Generate a unique Image ID for this build
    APP_VERSION=$(docker run --rm $IMAGE_LATEST /bin/bash /usr/local/bin/version)
    UNIQUE_ID=${APP_VERSION}_$(git rev-list --count HEAD)_$(git rev-parse --short HEAD)_${CI_PIPELINE_ID}
    IMAGE_UNIQUE=${IMAGE_REPOSITORY}:$UNIQUE_ID

}

function warning () { 

      local message=$1

      echo -e "\033[1;33mWARNING: $message\033[0m" 

}

function warnOnMattermost()
{
 
      local message=$1

      echo "notifying on Mattermost..."

      curl -s -X POST \
            -d "payload={\"username\": \"${MATTERMOST_USERNAME}\", \"channel\": \"${MATTERMOST_CHANNEL}\", \"text\": \"unlock::key::policeman: ci-cd-gateway **$CI_PROJECT_NAME**: $message [[Pipeline Logs](https://smarthub-wbench.wesp.telekom.net/gitlab/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME/-/jobs/${CI_JOB_ID})]\"}" \
            ${MATTERMOST_URL} > /dev/null

}


function notifyPromotor()
{

    local promotorUrl=$1
    local promotorToken=$2  

    curl $DEBUG_PARAM -s -k -X POST \
        -F token=$promotorToken \
        -F ref=master \
        -F "variables[DEPLOYMENT_ACTION]=UPDATE_VALUE" \
        -F "variables[CI_REGISTRY_IMAGE]=$CI_REGISTRY_IMAGE" \
        -F "variables[NEW_IMAGE_VERSION]=$UNIQUE_ID" \
        $promotorUrl 

} 


####### 2. - MAIN BLOCK ########################################################################################################################################

####### 2.1 - Default configuration and initialisation

ERROR=false
DEBUG_PARAM=""
[[ "$DEBUG_MODE" == "1" ]] && DEBUG_PARAM="-v"





####### 2.2. - Obtaing Application Docker Image's Tag : UniqueID

echo "Obtaing Application Docker Image's Tag : UniqueID"

generateImageUniqueID

####### 2.4. - Notifying promotor of new Version

ERROR=false

if [[ "${DEPLOY_ON}" == "1" ]]; then

    echo "${PROMOTOR_URL} ${PROMOTOR_TOKEN}" > /dev/null
    
    warning "\nNotifying Promotor of new Version: $UNIQUE_ID"

    [[ "$DEBUG_MODE" == "1" ]]  && warning "Using PROMOTOR_URL: ${PROMOTOR_URL}, PROMOTOR_TOKEN: ${PROMOTOR_TOKEN}. \n"

    notifyPromotor $ORANGE_PROMOTOR_URL $ORANGE_PROMOTOR_TOKEN || ERROR=true

    if [ "$ERROR" == "true" ]; then
        warnOnMattermost "${MATTERMOST_NOTIFY_FAILURES_TO} => Failed to notify ORANGE Promotor of new Version **$UNIQUE_ID.** "
    else
       [[ "${MATTERMOST_NOTIFY_ON_SUCCESS}" == "1" ]] && warnOnMattermost "**Orange Promotor** Notified  of new Version **$UNIQUE_ID**. "   
    fi
fi

