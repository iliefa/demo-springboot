package de.iliefa.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/hello")
public class DemoController{

    @GetMapping("/{name}")
    public String HelloWorl(@PathVariable("name") String name){
        return String.format("Hello %s",name);
    }
}