FROM openjdk:8-alpine
RUN apk --no-cache add curl
ENV JAVA_OPTS="-Xmx256m -Xms256m"
COPY target/demo.jar /opt/app.jar
COPY docker-entrypoint.sh /docker-entrypoint.sh
#COPY notify.sh /usr/local/bin
EXPOSE 8080

ENTRYPOINT ["sh", "/docker-entrypoint.sh"]
